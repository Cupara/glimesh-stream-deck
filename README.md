# Glimesh Stream Deck Plugin

A generic plugin for Stream Deck that allows you to see how many viewers you currently have and your total follower count. There will be more features but I wanted to start simple first.

# Open-Source and Absolutely Free

# Current Features
- Show current follower count
- Send messages to chat
