# Getting Started

### Creating Developer Application
1. Head on over to [Glimesh Profile Settings](https://glimesh.tv/users/settings/profile)
2. Find the Developers section in the left hand menu and click on Applications
3. Click Create Application
4. Give it a Name, Homepage, Description, Image if you want.
5. Allowed redirect URIs you will use this http://localhost:4545/success
6. Click Save Application

Take note of your Client ID and Secret Key on the new page that loads. You will use these in the plugin.
